package org.NASA.Core;
import java.util.Iterator;
import java.util.Scanner;

import org.NASA.Model.Robot;


public class MarsRoverController {

	public static void main(String[] args) {
		int totalRobots;
		Scanner in = new Scanner(System.in);;
		try {
			System.out.println("Enter width and breadth of Plateau");
			Plateau.initialiZePlateau(in.nextInt(), in.nextInt());
//			Plateau.initialiZePlateau(5, 5);
			System.out.println("Enter no of Robots");
			totalRobots=in.nextInt();

			for (int i = 0; i < totalRobots; i++) {
				int deployCoordinateX=in.nextInt();
				int deployCoordinateY=in.nextInt();
				char facingDirection= Character.toUpperCase( in.next().charAt(0));
				Robot robot = new Robot( deployCoordinateX,deployCoordinateY,facingDirection);
				in.nextLine();
				for (char cmd : in.nextLine().toCharArray()) {
					if (robot.isAlive()){
						robot.processCommand(cmd);
					}
				}
				if (robot.isAlive())
					robot.printCurrentLocation();

			}
			
		} catch (Exception e) {
			 e.printStackTrace();
		}
		
		in.close();
	}

}
