package org.NASA.Core;

public class Plateau {
	
	static char plateauBoard [][];
	static int plateauWidth=0;
	static int PlateauBreadth=0;
	static boolean isInitialized=false;
	static void initialiZePlateau(int x, int y){
		x++;
		y++;
		plateauWidth=x;
		PlateauBreadth=y;
		if ( !isInitialized ){
			plateauBoard = new char [x][y];
			for (int i=0;i<x;i++){
				for(int j=0;j<x;j++){
				plateauBoard[i][j]=' ';
				}
			}
			isInitialized=true;
		}
	}
	
	public static void setBeacon(int i, int j, char c){
		plateauBoard[i][j]=c;
	}
	
	public static char lookforBeaconAlerts(int i, int j){
		return plateauBoard[i][j];
	}

	public static int getPlateauBreadth() {
		return PlateauBreadth;
	}
	
	public static int getPlateauWidth() {
		return plateauWidth;
	}

}