package org.NASA.Model;

import org.NASA.Core.Plateau;

public class Robot {
	
	char facingDirection;
	int x;
	int y;
	boolean isAlive=true;
	char lastCommand=facingDirection;

	public Robot(int x, int y, char facingDirection) {
		this.x=x;
		this.y=y;
		this.facingDirection=facingDirection;
		this.isAlive=true;
	}
	
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}

	public char getFaceingDirection() {
		return facingDirection;
	}
	public void setFaceingDirection(char faceingDirection) {
		this.facingDirection = faceingDirection;
	}

	public boolean isAlive() {
		return isAlive;
	}

	public void setDead() {
		this.isAlive = false;
	}

	public void processCommand(char c){
	
		if (this.isAlive()){
			this.lastCommand=c;
			c=Character.toUpperCase(c);
			try{
				if (c=='M'){
					moveRover();
				}
				else  if (c=='L' || c=='R'){
					turnRover(c);
				}
				else{
					throw new Exception("Wrong Command instruction ["+c+ "] given to robots "); 
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}
	
	private void turnRover(char c ){
		
		switch (this.facingDirection) {
			
			case 'N':
				if (c=='L')
					this.facingDirection='W';
				else
					this.facingDirection='E';
				break;

			case 'E':
				if (c=='L')
					this.facingDirection='N';
				else
					this.facingDirection='S';
				break;
			case 'S':
				if (c=='L')
					this.facingDirection='E';
				else
					this.facingDirection='W';
				break;
			case 'W':
				if (c=='L')
					this.facingDirection='S';
				else
					this.facingDirection='N';
				break;
	
			default:
				break;
		}
	}

	private void moveRover(){
		
		if (this.facingDirection!=Plateau.lookforBeaconAlerts(this.getX(), this.getY())){
			if (this.facingDirection=='N'){
				moveNorth();
				deadStatus();
			}else if (this.facingDirection=='E'){
				moveEast();
				deadStatus();
			}else if (this.facingDirection=='S'){
				moveSouth();
				deadStatus();
			}else{
				moveWest();
				deadStatus();
			}
		}
	}
		
	private void moveNorth(){
		this.y+=1;
		
	}
	private void moveEast(){
		this.x+=1;
		
	}
	private void moveSouth(){
		this.y-=1;
		
	}
	private void moveWest(){
		this.x-=1;
	}
	
	private void deadStatus(){
	
		if (this.getX()<0){
			this.setDead();
			this.setX(this.getX()+1);
		}else if(this.getX()>Plateau.getPlateauWidth()-1){
			this.setDead();
			this.setX(this.getX()-1);
		}else if(this.getY()<0){
			this.setDead();
			this.setY(this.getY()+1);
		}else if(this.getY()>Plateau.getPlateauBreadth()-1){
			this.setDead();
			this.setY(this.getY()-1);
		}
		
		if (!this.isAlive()){
			printRIPLocation(this.getX(), this.getY());
			Plateau.setBeacon(this.getX(),this.getY(), this.facingDirection);
		}

	}

	public void printCurrentLocation(){
		System.out.println(this.x+ " " + this.y + " " + this.facingDirection);
	}
	
	public void printRIPLocation(int x, int y){
		System.out.println(x+ " " + y + " " + this.facingDirection+ " RIP");
	}

}